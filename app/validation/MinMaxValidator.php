<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 1/6/15
 * Time: 7:07 PM
 */

class MinMaxValidator extends  \Phalcon\Mvc\Model\Validator implements \Phalcon\Mvc\Model\ValidatorInterface {

    /**
     * Validation min max value
     * @param $model
     * @return bool
     */
    public function validate($model) {
        $field = $this->getOption('field');
        $min = $this->getOption('min');
        $max = $this->getOption('max');
        $msg = $this->getOption('msg');

        $value = $this->getOption('value') ?  $this->getOption('value') : $model->$field;

        if((int)$value < (int)$min || (int)$value > (int)$max) {
            $this->appendMessage(
                $msg ? $msg : $field . ' must from ' . $min . ' to ' . $max,
                $field,
                'MinMaxValidator'
            );
            return false;
        }
        return true;
    }
}