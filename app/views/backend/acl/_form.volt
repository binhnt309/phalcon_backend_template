{% set level = ['1' : 'Level 1', '2' : 'Level 2', '3' : 'Level 3'] %}

<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'frm_acl', 'role' : 'form') }}
        <h4>{{ t._('Controller & action name required for ACL') }}</h4>
        <div class="form-group">
            <label>{{ t._('Controller name') }}:</label>
            {{ text_field('controller', 'class' : 'form-control', 'value' : (req.getPost('controller') ? req.getPost('controller') : acl.getController() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Action name') }}:</label>
            {{ text_field('action', 'class' : 'form-control', 'value' : (req.getPost('action') ? req.getPost('action') : acl.getAction() )) }}
        </div>

        <hr>
        <h4>{{ t._('Data for display menu') }}</h4>
        <div class="form-group">
            <label>{{ t._('Label') }}:</label>
            {{ text_field('label', 'class' : 'form-control', 'value' : (req.getPost('label') ? req.getPost('label') : acl.getLabel() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Parent Menu') }}</label>
            {{ select('parent_id', acl_arr, 'useEmpty': true, 'emptyText' : '-- Select parent menu --', 'emptyValue' : 0, 'class' : 'form-control', 'value' : acl.getParentId() ) }}
            {{ select('level', level, 'class' : 'form-control hidden', 'value' : acl.getLevel()) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Class name') }}:</label>
            {{ text_field('class', 'class' : 'form-control', 'value' : (req.getPost('class') ? req.getPost('class') : acl.getClass() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Description') }}:</label>
            <textarea class="form-control" name="description">{{ acl.getDescription() }}</textarea>
        </div>

        <div class="form-group">
            <label>{{ t._('Position') }}:</label>
            {{ text_field('pos', 'class' : 'form-control', 'value' : (req.getPost('pos') ? req.getPost('pos') : acl.getPos() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Enable for menu') }}:</label>
            {{ check_field('state', 'value' : acl.getState(), 'title' : t._('Check this if want display in menu') ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ acl.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<style type="text/css">
    #state { position: relative; top: 3px; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=frm_acl]').prop('action', window.location.href);

        $('#state').prop('checked', parseInt($('#state').val())).change(function() {
            $(this).val($('#state').prop('checked') ? 1 : 0);
        });

        $('#parent_id').unbind().change(function() {
            if($(this).find('option:selected').text().indexOf('---') >= 0) {
                $('#level').val('<?php echo AclExt::Level_3 ?>');
            } else if($(this).val() == '0') {
                $('#level').val('<?php echo AclExt::Level_1 ?>');
            } else {
                $('#level').val('<?php echo AclExt::Level_2 ?>');
            }
        });
    });
</script>