<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('Edit ACL') }}</h1>
        <a href="{{ url('admin/acl/index') }}">
            <button class="btn btn-primary" type="button">{{ t._('Back to list') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>
{{ partial('acl/_form', {'acl': acl, 'acl_arr' : acl_arr }) }}