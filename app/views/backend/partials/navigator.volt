{% set logger = session.get('member') %}
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('admin/') }}">{{ t._('Welcome') ~ ' ' ~ '<b>'~logger['username']~'</b>' }}</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li>
            <a href="javascript:;" class="current-time">
                <span><?php echo date('Y-m-d H:i:s') ?></span>
            </a>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="{{ url.getBaseUri() }}admin/user/edit/{{ session.get('member')['id'] }}">
                        <i class="fa fa-user fa-fw"></i>{{ t._('User Profile') }}
                    </a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url.getBaseUri() }}admin/auth/logout"><i class="fa fa-sign-out fa-fw"></i> {{
                    t._('Logout')
                    }}</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{ url('admin/') }}"><i class="fa fa-dashboard fa-fw"></i> {{ t._('Dashboard') }}</a>
                </li>
                <li>
                    <a href="#"><i class="glyphicon glyphicon-cog"></i> {{ t._('System') }}<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ url('admin/system/index') }}">{{ t._('Config') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/acl/index') }}">{{ t._('List ACL') }}</a>
                        </li>
                    </ul>
                </li>
                <!--<li>
                    <a href="#">
                        <i class="glyphicon glyphicon-user"></i> {{ t._('User Manage') }}
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ url('admin/user/index') }}">{{ t._('Users') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/user_group/index') }}">{{ t._('User Group') }}</a>
                        </li>
                    </ul>
                </li>-->
                {% for m in menu_list %}
                    {% set data = m['data'] %}
                    {% set level_2 = m['child']|length ? true : false %}
                    <li>
                        <a href="{{ level_2 ? '#' : url('admin/' ~ data.controller ~ '/' ~ data.action)}}">
                            <i class="{{ data.class }}"></i> {{ t._(data.label) }}
                            {% if level_2 %}
                            <span class="fa arrow"></span>
                            {% endif %}
                        </a>
                        {% if level_2 %}
                        <ul class="nav nav-second-level">
                            {% for child in m['child'] %}
                            {% set level_3 = child['child'] is defined ? (child['child']|length ? true : false) : false %}
                            <li>
                                <a href="{{ level_3 ? '#' : url('admin/' ~ child.controller ~ '/' ~ child.action) }}">{{ t._(child.label) }}</a>
                                {% if level_3 %}
                                <ul class="nav nav-third-level">
                                    {% for sub_child in child['child'] %}
                                    <li>
                                        <a href="{{ url('admin/' ~ sub_child.controller ~ '/' ~ sub_child.action) }}">{{ t._(sub_child.label) }}</a>
                                    </li>
                                    {% endfor %}
                                </ul>
                                {% endif %}
                            </li>
                            {% endfor %}
                        </ul>
                        {% endif %}
                    </li>
                {% endfor %}
            </ul>
            <!-- /#side-menu -->
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

<style type="text/css">
    li a.current-time:hover, li a.current-time:active  {
        background-color: inherit;
    }
</style>
<script type="text/javascript">
    var seconds = <?php echo date('s') ?>,
        minutes = <?php echo date('i') ?>,
        hours = <?php echo date('G') ?>,
        date = <?php echo date('j') ?>,
        month = <?php echo date('n') ?>,
        year = <?php echo date('Y') ?>,
        inc_ = 0;

    setInterval(function() {
        inc_ = inc_ + 1;
        var _date = new Date(year, month - 1, date, hours, minutes, seconds + inc_);
        $('.current-time span').html(
            _date.getFullYear() + '-'
            + (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1) : (_date.getMonth() + 1)) + '-'
            + (_date.getDate() < 10 ? '0' + _date.getDate() : _date.getDate()) + ' '
            + (_date.getHours() < 10 ? '0' + _date.getHours() : _date.getHours()) + ':'
            + (_date.getMinutes() < 10 ? '0' + _date.getMinutes(): _date.getMinutes()) + ':'
            + (_date.getSeconds() < 10 ? '0' + _date.getSeconds() : _date.getSeconds())
        );
    } , 1000);
</script>