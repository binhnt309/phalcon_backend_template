<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('System config') }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
    </div>
</div>