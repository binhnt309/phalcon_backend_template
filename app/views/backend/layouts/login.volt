<!DOCTYPE html>
<html ng-app="email-app">
<head>
    <meta charset="UTF-8">
    {{ get_title() }}
    {{ assets.outputCss('loginCss') }}
    {{ assets.outputJs('loginJs') }}
</head>
<body>
<div class="container">
    {{ content() }}
</div>
</body>
</html>