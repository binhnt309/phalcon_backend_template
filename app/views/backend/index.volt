<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ get_title() }}
    {{ assets.outputCss('backendCss') }}
    {{ assets.outputJs('headJs') }}
</head>
<body>
    <div class="wrapper">

        {{ partial('partials/navigator') }}

        <div id="page-wrapper">
            {{ content() }}
        </div>

    </div>

    {{ assets.outputJs('backendJs') }}
</body>

</html>