<?php

class System extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $key;

    /**
     *
     * @var string
     */
    protected $value;

    /**
     * Method to set the value of field key
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'key' => 'key', 
            'value' => 'value'
        );
    }

}
