<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class AclExt extends \Acl {

    const Level_1 = 1;
    const Level_2 = 2;
    const Level_3 = 3;

    const STATE_DISABLED = 0;
    const STATE_ENABLED = 1;

    public function getSource() {
        return 'acl';
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => self::STATUS_DELETED
            ]
        ));*/
    }

    public function beforeValidation() {

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'controller', 'action'
            ],
            'message' => 'Controller and Action name must be unique!'
        ]));

        /*$this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'controller',
            'message' => 'Controller name is required!'
        ]));
        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'action',
            'message' => 'Action name is required!'
        ]));*/

        return $this->validationHasFailed() != true;
    }

}