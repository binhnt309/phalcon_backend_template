<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/12/14
 * Time: 2:04 PM
 */

class UserExt extends User {

    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_DENIED = 'DENIED';
    const STATUS_DELETED = 'DELETED';

    public function getSource() {
        return 'user';
    }

    static function getTable() {
        $instance = new User();
        return $instance->getSource();
    }

    public function initialize() {
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));*/
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => self::STATUS_DELETED
            ]
        ));
    }

    public function beforeValidation() {

        $request = new Phalcon\Http\Request();

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => 'username',
            'message' => 'Username must be unique!'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => 'email',
            'message' => 'Email must be unique!'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'group',
            'message' => 'Group must required!'
        ]));

        if(strlen($request->get('pwd')) > 0 & $this->id) {
            $this->validate(new MinMaxValidator([
                'field' => 'pwd',
                'min' => 6,
                'max' => 20,
                'msg' => 'Password must greater or equal 6',
                'value' => strlen($request->get('pwd'))
            ]));
        }

        return $this->validationHasFailed() != true;
    }

    public function beforeSave() {
        /*$request = new Phalcon\Http\Request();
        if(strlen($request->get('pwd')) < 6) {
            $this->appendMessage(new \Phalcon\Mvc\Model\Message(
               'Length of password must be greater than 6', 'pwd', 'MinMaxValidation', 'code'
            ));
            $valid = false;
        }

        return $valid;*/
    }

}