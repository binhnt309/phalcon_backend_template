<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class UserGroupExt extends UserGroup {

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    public function getSource() {
        return 'user_group';
    }

    public function initialize() {

    }

    public function beforeValidation() {

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => 'name',
            'message' => 'Group name must be unique!'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'name',
            'message' => 'Group name is required!'
        ]));

        return $this->validationHasFailed() != true;
    }
}