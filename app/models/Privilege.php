<?php

class Privilege extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $group_acl_id;

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field group_acl_id
     *
     * @param integer $group_acl_id
     * @return $this
     */
    public function setGroupAclId($group_acl_id)
    {
        $this->group_acl_id = $group_acl_id;

        return $this;
    }

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field group_acl_id
     *
     * @return integer
     */
    public function getGroupAclId()
    {
        return $this->group_acl_id;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    public function setAclId($acl_id)
    {
        $this->acl_id = $acl_id;

        return $this;
    }
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;

        return $this;
    }
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;

        return $this;
    }
    public function setModifiedTime($modified_time)
    {
        $this->modified_time = $modified_time;

        return $this;
    }
    public function getAclId()
    {
        return $this->acl_id;
    }
    public function getGroupId()
    {
        return $this->group_id;
    }
    public function getCreatedTime()
    {
        return $this->created_time;
    }
    public function getModifiedTime()
    {
        return $this->modified_time;
    }
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'acl_id' => 'acl_id', 
            'user_id' => 'user_id', 
            'group_id' => 'group_id', 
            'created_time' => 'created_time', 
            'modified_time' => 'modified_time'
        );
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'group_acl_id' => 'group_acl_id', 
            'user_id' => 'user_id'
        );
    }

}
