<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/22/14
 * Time: 11:03 AM
 */
class ModelHelper
{
    /**
     * Convert list object Model to array
     * @param $data
     * @param $key
     * @return array
     */
    static function toArray($data, $key = null)
    {
        $result = [];
        if (count($data) > 0)
            foreach ($data as $inc => $obj) {
                if (isset($obj->$key)) {
                    $inc = $obj->$key;
                }
                $result[$inc] = $obj->toArray();
            }
        return $result;
    }

}