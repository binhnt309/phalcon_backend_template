<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/15/14
 * Time: 10:02 AM
 */
namespace Application\Controllers\Backend;

class AclController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
        //$this->__checkPermission();
    }

    /**
     * View list user
     */
    public function indexAction()
    {
        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

        // Load ACL list
        $acl = \AclExt::find();

        $this->view->setVars([
            'acl' => $acl
        ]);
    }

    public function createAction()
    {
        // Check permission
        if ($this->session->get('member')['username'] != 'admin') {
            $this->view->disable();
            $this->flashSession->warning($this->_getTranslation()->_('You have not permission execute this action'));
            $this->response->redirect('admin/user/index');
        } // ------------

        $acl_list = \AclExt::find([
            "conditions" => "level <= " . \AclExt::Level_2,
            'order' => 'level, pos ASC'
        ]);
        $acl_arr = [];
        if(!empty($acl_list)) {
            foreach($acl_list as $_acl) {
                if($_acl instanceof \AclExt) {
                    if($_acl->getLevel() == 1) {
                        $acl_arr[$_acl->getId()] = $_acl->getLabel() . " ({$_acl->getController()}/{$_acl->getAction()})";
                        continue;
                    }
                    if (in_array($_acl->getParentId(), $acl_arr)) {
                        $acl_arr[$_acl->getId()] = '--- ' . $_acl->getLabel() . " ({$_acl->getController()}/{$_acl->getAction()})";
                    }
                }
            }
        }

        // Post create new memeber
        if ($this->request->isPost()) {
            $acl = new \AclExt();

            $acl->setLabel($this->request->getPost('label'));
            $acl->setLevel($this->request->getPost('level'));
            $acl->setParentId($this->request->getPost('parent_id'));
            $acl->setPos($this->request->getPost('pos'));
            $acl->setClass($this->request->getPost('class'));
            $acl->setController($this->request->getPost('controller'));
            $acl->setAction($this->request->getPost('action'));
            $acl->setDescription($this->request->getPost('description'));
            $acl->setState($this->request->getPost('state'));

            if ($acl->save()) {
                $this->view->disable(); // Option to display flash message
                $this->flashSession->success($this->_getTranslation()->_('Create ACL  success!'));
                $this->response->redirect('admin/acl/index');
            } else {
                // Save false
                $_msg = '';
                if ($acl->getMessages()) {
                    foreach ($acl->getMessages() as $msg) {
                        $messages[$msg->getField()] = $msg->getMessage();
                        $_msg .= '<br/> - ' . $msg->getMessage();
                    }
                }
                $this->flashSession->error($this->_getTranslation()->_('Create ACL error!') . $_msg);
            }
        }

        $this->view->setVars([
                'acl' => new \AclExt(),
                'req' => $this->request, 'acl_arr' => $acl_arr
            ]
        );
    }

    public function updateAction($id)
    {
        $this->editAction($id);
    }

    public function editAction($id)
    {
        $acl = \AclExt::findFirst("id=$id");

        $acl_list = \AclExt::find([
            "conditions" => "level <= " . \AclExt::Level_2,
            'order' => 'level, pos ASC'
        ]);
        $acl_arr = [];
        if(!empty($acl_list)) {
            foreach($acl_list as $_acl) {
                if($_acl instanceof \AclExt) {
                    if($_acl->getLevel() == 1) {
                        $acl_arr[$_acl->getId()] = $_acl->getLabel() . " ({$_acl->getController()}/{$_acl->getAction()})";
                        continue;
                    }
                    $acl_arr[$_acl->getId()] = '--- ' . $_acl->getLabel() . " ({$_acl->getController()}/{$_acl->getAction()})";
                }
            }
        }

        if ($this->request->isPost() & $acl instanceof \AclExt ) {
            $acl->setLabel($this->request->getPost('label'));
            $acl->setLevel($this->request->getPost('level'));
            $acl->setParentId($this->request->getPost('parent_id'));
            $acl->setPos($this->request->getPost('pos'));
            $acl->setClass($this->request->getPost('class'));
            $acl->setController($this->request->getPost('controller'));
            $acl->setAction($this->request->getPost('action'));
            $acl->setDescription($this->request->getPost('description'));
            $acl->setState($this->request->getPost('state'));

            if ($acl->save()) {
                $this->view->disable(); // Option to display flash message
                $this->flashSession->success($this->_getTranslation()->_('Update success!'));
                $this->response->redirect('admin/acl/index');
            } else {
                // Update fail
                $_msg = '';
                if ($acl->getMessages()) {
                    foreach ($acl->getMessages() as $msg) {
                        $messages[$msg->getField()] = $msg->getMessage();
                        $_msg .= '<br/> - ' . $msg->getMessage();
                    }
                }
                $this->flashSession->error($this->_getTranslation()->_('Update error!') . $_msg);
            }
        }

        if (!$acl) {
            $acl = new \AclExt();
            $acl->setId(0);
        }

        $this->view->setVars(
            ['acl' => $acl, 'req' => $this->request, 'acl_arr' => $acl_arr]
        );
    }

    /**
     * Delete member
     * @param $id
     */
    public function deleteAction($id)
    {
        $this->view->disable(); // Option to display flash message
        $acl = \AclExt::findFirst($id);
        if ($acl) {
            if (!$acl->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }

        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/acl/index');
    }

    public function ajaxAction()
    {

        $this->view->disable();

        $arr_fields = array(
            'id',
            'full_name',
            'username',
            'group',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";; //status != '" . \BaseMember::STATUS_DELETED . "'
        if (!empty($key)) {
            $condition .= " AND username LIKE '%{$key}%' OR full_name LIKE '%{$key}%'";
        }

        $total = \UserExt::count(array('conditions' => $condition));

        $member = \UserExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));
        $result = array();
        $groups = \ModelHelper::toArray(\UserGroupExt::find());
        $user_group = isset($groups[$this->session->get('member')['group']]) ? $groups[$this->session->get('member')['group']]['name'] : '';

        foreach ($member as $key => $m) {
            if ($m instanceof \User) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getFullName();
                $result[$key][] = $m->getUsername();
                $result[$key][] = isset($groups[$m->getGroup()]) ? $groups[$m->getGroup()]['name'] : '---';
                $result[$key][] = $m->getStatus();
                $result[$key][] = $m->getStatus() != \UserExt::STATUS_DELETED ?
                    ($this->session->get('member')['username'] == 'admin' || $m->getId() == $this->session->get('member')['id'] ?
                        "<a href='/admin/user/edit/{$m->getId()}'>" . $this->_getTranslation()->_('Edit')
                        . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/user/delete/{$m->getId()}'>"
                        . $this->_getTranslation()->_('Delete') . "</a>"
                        : "")
                    : '';
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    private function __checkPermission() {
        if ($this->session->get('member')['username'] != 'admin') {
            $this->view->disable();
            $this->flashSession->warning($this->_getTranslation()->_('You have not permission execute this action!'));

            $backUrl = $this->request->getHeader('REFERER');
            if(empty($backUrl)) {
                $backUrl = 'admin/';
            }
            $this->response->redirect($backUrl);
        }
    }
}