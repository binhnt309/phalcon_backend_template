<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 4:44 PM
 */
namespace Application\Controllers\Backend;

class UserGroupController extends ControllerBase
{

    public function indexAction()
    {

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

        // Load group
        $group = \UserGroupExt::find();

        $this->view->setVars([
            'group' => $group
        ]);
    }

    public function createAction()
    {

        if ($this->request->isPost()) {
            $group = new \UserGroupExt();
            $group->name = $this->request->getPost('name');
            $group->description = $this->request->getPost('description');
            $group->status = $this->request->getPost('status');

            if ($group->save()) {
                // Create access list control for current group
                $acl_post = $this->request->getPost('acl');
                $current_acl = $this->request->getPost('acl_of_group');
                if (!empty($acl_post) & $acl_post != $current_acl) {
                    // Empty current list acl of this group
                    $group_acl = new \UserGroupAclExt();
                    if (!empty($current_acl)) {
                        foreach ($current_acl as $_acl) {
                            if (in_array($_acl, $acl_post)) {
                                unset($acl_post[array_search($_acl, $acl_post)]);
                                unset($current_acl[array_search($_acl, $current_acl)]);
                            }
                        }

                        if (!empty($current_acl)) // Find list acl are deleted
                            $this->db->query("DELETE FROM {$group_acl->getSource()} WHERE `acl_id` IN (" . implode(',', $current_acl) . ") AND `group_id`={$id}");
                    }
                    if (!empty($acl_post)) {
                        $sql = "INSERT INTO {$group_acl->getSource()}(`acl_id`, `group_id`, `created_time`, `modified_time`) VALUES";
                        $comma = '';
                        foreach ($acl_post as $_acl) {
                            $time = date('Y-m-d H:i:s');
                            $sql .= $comma . "({$_acl}, {$group->id}, '{$time}', '{$time}')";
                            $comma = ',';
                        }
                        $this->db->query($sql); //---------------------
                    }
                }
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create group success'));
                $this->response->redirect('admin/user_group/index');
            } else {
                $_msg = '';
                if ($group->getMessages()) {
                    foreach ($group->getMessages() as $msg) {
                        $messages[$msg->getField()] = $msg->getMessage();
                        $_msg .= '<br/> - ' . $msg->getMessage();
                    }
                }
                $this->flashSession->error($this->_getTranslation()->_('Create error! Please read messages below.') . $_msg);
            }
        } // End if: Create new user group

        // Load list acl
        $acl_list = [];
        $acl_selected = [];
        $acl = \AclExt::find(['conditions' => "controller != '' AND action != ''"]);
        if (count($acl)) {
            foreach ($acl as $a) {
                $acl_list[$a->getId()] = $a->getController() . '/' . $a->getAction();
            }
        }
        $acl_of_group = \UserGroupAclExt::find([
            'conditions' => "group_id=0"
        ]);
        $acl_of_group_selected = [];
        $this->view->setVars([
            'group' => new \UserGroup(),
            'req' => $this->request,
            'acl_list' => $acl_list,
            'acl_selected' => $acl_selected,
            'acl_of_group' => $acl_of_group,
            'acl_of_group_selected' => $acl_of_group_selected
        ]);
    }

    public function editAction($id)
    {
        if (empty($id)) {
            $id = 0;
        }
        $group = \UserGroupExt::findFirst($id);

        if ($this->request->isPost() & $group instanceof \UserGroup) {
            $group->name = $this->request->getPost('name');
            $group->description = $this->request->getPost('description');
            $group->status = $this->request->getPost('status');

            if ($group->save()) {
                // Create access list control for current group
                $acl_post = $this->request->getPost('acl');
                $current_acl = $this->request->getPost('acl_of_group');
                if (!empty($acl_post) & $acl_post != $current_acl) {
                    // Empty current list acl of this group
                    $group_acl = new \UserGroupAclExt();
                    if (!empty($current_acl)) {
                        foreach ($current_acl as $_acl) {
                            if (in_array($_acl, $acl_post)) {
                                unset($acl_post[array_search($_acl, $acl_post)]);
                                unset($current_acl[array_search($_acl, $current_acl)]);
                            }
                        }

                        if (!empty($current_acl)) // Find list acl are deleted
                            $this->db->query("DELETE FROM {$group_acl->getSource()} WHERE `acl_id` IN (" . implode(',', $current_acl) . ") AND `group_id`={$id}");
                    }
                    if (!empty($acl_post)) {
                        $sql = "INSERT INTO {$group_acl->getSource()}(`acl_id`, `group_id`, `created_time`, `modified_time`) VALUES";
                        $comma = '';
                        foreach ($acl_post as $_acl) {
                            $time = date('Y-m-d H:i:s');
                            $sql .= $comma . "({$_acl}, {$group->id}, '{$time}', '{$time}')";
                            $comma = ',';
                        }
                        $this->db->query($sql); //---------------------
                    }
                }

                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Update group success'));
                $this->response->redirect('admin/user_group/index');
            } else {
                $_msg = '';
                if ($group->getMessages()) {
                    foreach ($group->getMessages() as $msg) {
                        $messages[$msg->getField()] = $msg->getMessage();
                        $_msg .= '<br/> - ' . $msg->getMessage();
                    }
                }
                $this->flashSession->error($this->_getTranslation()->_('Create error!') . $_msg);
            }
        } // --------

        if (!$group) {
            $group = new \UserGroup();
        }

        // Load list acl
        $acl_list = [];
        $acl_data = \AclExt::find(['conditions' => "controller != '' AND action != ''"]);
        if (count($acl_data)) {
            foreach ($acl_data as $a) {
                $acl_list[$a->getId()] = $a->getController() . '/' . $a->getAction();
            }
        }
        // Load list acl in group
        $acl_group = \UserGroupAclExt::find([
            'conditions' => "group_id={$id}"
        ]);
        $acl_selected = [];
        if (count($acl_group)) {
            foreach ($acl_group as $g) {
                $acl_selected[] = $g->getAclId();
            }
        }

        // Find current acl of group
        $acl_of_group = \UserGroupAclExt::find([
            'conditions' => "group_id={$id}"
        ]);
        $acl_of_group_selected = [];
        foreach ($acl_of_group as $_acl) {
            if ($_acl instanceof \UserGroupAclExt)
                $acl_of_group_selected[] = $_acl->getAclId();
        }

        $this->view->setVars([
            'group' => $group,
            'req' => $this->request,
            'acl_list' => $acl_list,
            'acl_selected' => $acl_selected,
            'acl_of_group' => $acl_of_group,
            'acl_of_group_selected' => $acl_of_group_selected
        ]);
    }

    public function deleteAction($id = 0)
    {
        $this->view->disable();

        $group = \UserGroupExt::findFirst($id);
        if($group) {
            if ($group->delete()) {
                // Delete all acl of group
                $acl_of_group = new \UserGroupAclExt();
                $this->db->query("DELETE FROM `" . $acl_of_group->getSource() . "` WHERE `group_id` = " . $group->id);

                $this->flashSession->success($this->_getTranslation()->_('Delete group success!'));
            } else {
                $this->flashSession->error($this->_getTranslation()->_('Delete group error!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Group not exist!'));
        }

        $this->response->redirect('admin/user_group/index');
    }
}