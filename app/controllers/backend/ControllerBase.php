<?php

namespace Application\Controllers\Backend;

use Phalcon\Exception;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    public function initialize()
    {
        // Default title
        $this->tag->setTitle("CPanel");
        $this->view->setVar('t', $this->_getTranslation());

        // Assets manager: Js and Css
        $this->assets->collection('backendCss')->addCss('assets/css/bootstrap.min.css', true)->addCss('assets/font-awesome/css/font-awesome.css', true)
            ->addCss('assets/css/sb-admin.css', true)->addCss('assets/css/email-backend.css', true);

        $this->assets->collection('headJs')->addJs('assets/js/jquery-1.10.2.js', true)->addJs('assets/js/bootstrap.min.js', true);

        $this->assets->collection('backendJs')->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js', true)
            //->addJs('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular.min.js', false)
            ->addJs('assets/js/sb-admin.js', true)->addJs('assets/js/active-menu.js', true)->addJs('assets/js/helper.js', true);

        try {
            $controller = $this->router->getControllerName();
            $action = $this->router->getActionName();

            // Require logged
            if ($this->session->get('isAuth')) {
                // Check permission
                if ($this->session->get('member')['username'] != 'admin' & !in_array($controller, [null, 'index'])) {
                    $this->__checkPermission();
                } // End check permission ------------

                // Load list menu
                $this->__loadMenu();
            } else {
                if ($controller != 'auth' && $action != 'login') {
                    $this->flashSession->output();

                    $this->view->disable();
                    $url = $this->router->getRewriteUri();
                    if (substr($url, 0, 1) == '/') {
                        $url = substr($url, 1);
                    }
                    $this->response->redirect('admin/auth/login?returnUrl=' . base64_encode($url));
                }
            }
        } catch (Exception $e) {
        }
    }

    public function _getTranslation()
    {
        $messages = array();
        // Get language
        $language = $this->request->get('_l');
        if (!$language) {
            $language = $this->session->get('_l');
            if (!$language) {
                $language = $this->request->getBestLanguage();
            }
        }
        $this->session->set('_l', $language);

        //Check if we have a translation file for that lang
        if (file_exists(__DIR__ . "/../../../app/messages/" . $language . ".php")) {
            require __DIR__ . "/../../../app/messages/" . $language . ".php";
        } else {
            // fallback to some default
            if (file_exists(__DIR__ . "/../../../app/messages/en.php"))
                require __DIR__ . "/../../../app/messages/en.php";
        }

        //Return a translation object
        return new \Phalcon\Translate\Adapter\NativeArray(array(
            "content" => $messages
        ));

    }

    public function afterExecuteRoute()
    {
        $this->view->setViewsDir($this->view->getVIewsDir() . 'backend/');
    }

    /**
     * Check permission of this logger on controller/action
     */
    private function __checkPermission()
    {
        if (empty($this->session->get('acl'))) {
            $acl_list = $this->__loadListAcl();
            $this->session->set('acl', $acl_list);
        } else {
            $acl_list = $this->session->get('acl');
        }
        $controller = $this->router->getControllerName();
        $action = $this->router->getActionName();

        $hasPermission = false;
        if (!empty($acl_list)) {
            foreach ($acl_list as $acl) {
                if ($acl['controller'] == $controller & $acl['action'] == $action) {
                    $hasPermission = true;
                    break;
                }
            }
        }
        if (!$hasPermission) {
            $this->view->disable();
            $backUrl = $this->request->getHeader('REFERER');
            if (empty($backUrl)) {
                $backUrl = 'admin/';
            }
            $this->flashSession->warning('You have not permission execute this action!');
            $this->response->redirect($backUrl);
        }
    }

    /**
     * Load lít Acl for current permission
     * @return array
     */
    private function __loadListAcl()
    {
        $acl_list = [];
        $group_id = $this->session->get('member')['group'];
        // Load list ACL first time access application
        $acl_of_group = \UserGroupAclExt::find([
            'conditions' => "group_id = {$group_id}"
        ]);
        $acl_ids = [];
        if (count($acl_of_group)) {
            foreach ($acl_of_group as $_acl) {
                $acl_ids[] = $_acl->getAclId();
            }

            // Read controller and action name of ACL
            $acl = \AclExt::find([
                'conditions' => "id IN (" . implode(',', $acl_ids) . ")"
            ]);

            foreach ($acl as $k => $_acl) {
                if ($_acl instanceof \AclExt) {
                    $acl_list[$k] = [];
                    $acl_list[$k]['controller'] = $_acl->getController();
                    $acl_list[$k]['action'] = $_acl->getAction();
                }
            }
        }
        return $acl_list;
    }

    private function __loadMenu()
    {

        try {
            $menus = \AclExt::find([
                'columns' => 'id, parent_id, label, controller, action, class, level, pos',
                'conditions' => "state = " . \AclExt::STATE_ENABLED,
                'order' => "level, pos ASC"
            ]);
        } catch (\Exception $e) {
            var_dump($e);
        }

        $menu_list = [];
        if (count($menus)) {
            foreach ($menus as $m) {
                $menu_list[$m->id]['data'] = $m;
                $menu_list[$m->id]['child'] = [];
            }
            /*
             * Re-sort menu with parent-child
             */
            // Level 3
            foreach ($menu_list as $m) {
                if ($m['data']->level == 3) {
                    if (array_key_exists($m['data']->parent_id, $menu_list)) {
                        $menu_list[$m['data']->parent_id]['child'][] = $m['data'];
                        unset($menu_list[$m['data']->id]);
                    }
                }
            }
            // Level 2
            foreach ($menu_list as $m) {
                if (array_key_exists($m['data']->parent_id, $menu_list)) {
                    $menu_list[$m['data']->parent_id]['child'][$m['data']->id] = $m['data'];
                    unset($menu_list[$m['data']->id]);
                }
            }
        }

        $this->view->setVar('menu_list', $menu_list);
        /*echo '<pre>';
        print_r($menu_list); die();*/
    }

}