/**
 * Created by binhnt on 12/17/14.
 */
$(document).ready(function() {
    var domain = document.domain,
        active_menu = false,
        obj_active = null;

    $('#side-menu a').each(function(index) {
        if(domain + $(this).attr('href') == window.location.href.replace(/http:\/\//, '')) {
            $(this).addClass('active');
            $(this).parent().parent().addClass('in').parent().addClass('active');
            if($(this).parent().parent().hasClass('nav-third-level')) {
                $(this).parent().parent().parent().parent().addClass('in').parent().addClass('active');
            }
            active_menu = true;
            return false;
        }
        if(domain + $(this).attr('href').replace(/index/, '')
            == window.location.href.replace(/http:\/\//, '').replace(/create|edit(.?)+/, ''))
        {
            obj_active = $(this);
        }
    });

    if(!active_menu) {
        $(obj_active).addClass('active');
        $(obj_active).parent().parent().addClass('in').parent().addClass('active');
        if($(obj_active).parent().parent().hasClass('nav-third-level')) {
            $(obj_active).parent().parent().parent().parent().addClass('in').parent().addClass('active');
        }
    }
});