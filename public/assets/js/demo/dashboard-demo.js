$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2010 Q1',
            sent: 2666,
            clicks: null,
            open: 2647
        }, {
            period: '2010 Q2',
            sent: 2778,
            clicks: 2294,
            open: 2441
        }, {
            period: '2010 Q3',
            sent: 4912,
            clicks: 1969,
            open: 2501
        }, {
            period: '2010 Q4',
            sent: 3767,
            clicks: 3597,
            open: 5689
        }, {
            period: '2011 Q1',
            sent: 6810,
            clicks: 1914,
            open: 2293
        }, {
            period: '2011 Q2',
            sent: 5670,
            clicks: 4293,
            open: 1881
        }, {
            period: '2011 Q3',
            sent: 4820,
            clicks: 3795,
            open: 1588
        }, {
            period: '2011 Q4',
            sent: 15073,
            clicks: 5967,
            open: 5175
        }, {
            period: '2012 Q1',
            sent: 10687,
            clicks: 4460,
            open: 2028
        }, {
            period: '2012 Q2',
            sent: 8432,
            clicks: 5713,
            open: 1791
        }],
        xkey: 'period',
        ykeys: ['sent', 'clicks', 'open'],
        labels: ['sent', 'clicks', 'open'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Open",
            value: 8900
        }, {
            label: "Clicks",
            value: 6799
        }, {
            label: "Bounces",
            value: 20
        }, {
            label: "Rejects",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2010',
            a: 50,
            b: 40
        }, {
            y: '2011',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });

});
